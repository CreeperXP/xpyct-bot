### XpyctBot 

![Discord](https://img.shields.io/discord/820223886843052053?label=Discord&logo=Discord) ![Website](https://img.shields.io/website?down_color=red&down_message=offline&logo=replit&up_color=green&up_message=online&url=https%3A%2F%2Fxpyctbot.dima47452.repl.co) ![GitHub last commit](https://img.shields.io/github/last-commit/CreeperXP/creeper_bot?logo=GitHub)

This bot is based on [Nextcord](https://github.com/nextcord/nextcord "Nextcord") and uses Python as the main language.

### About

The bot is made for entertainment, so have fun!
[Invite Bot](https://discord.com/oauth2/authorize?client_id=833720975069282344&scope=bot%20applications.commands "Invie Bot")

Matrix: *@creeperxp:converser.eu*

### License

![GNU version 3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)

This program is licensed under GNU General Public License, Version 3